cmake_minimum_required(VERSION 3.16)
project(03_classes)

set(CMAKE_CXX_STANDARD 17)

add_executable(03_classes main.cpp)