#include <iostream>

using namespace std;

void Cal() {
    static int count = 1;
    static int x = 1;
    x = count * count;
    cout << count << "*" << count;
    cout << ": " << x << endl;
    count++;
}

// this keyword
class PrintName {
    string name;

public:
    void set_name(const string &name) {
        this->name = name;
    }

    void print_name() {
        cout << this->name << "! Welcome to the C++ community :)" << endl;
    }
};

// aggregate example
struct Rectangle {
    int length;
    int width;
};

// constructors and destructors
class Coordinates {
public:
    Coordinates() {
        cout << "Constructor called!" << endl;
    }
    ~Coordinates() {
        cout << "Destructor called!" << endl;
    }
};

// friend declaration
class Height {
    double inches;

public:
    explicit Height(double value): inches(value) {}

    friend void print_feet(Height);
};

void print_feet(Height h) {
    cout << "Your height in inches is: " << h.inches << endl;
    cout << "Your height in feet is: " << h.inches * 0.083 << endl;
}

int main() {
    for (int i = 1; i < 11; i++) {
        Cal();
    }

    PrintName object;
    object.set_name("Edu");
    object.print_name();

    Rectangle rectangle = {10, 15};
    cout << rectangle.length << "," << rectangle.width << endl;

    Coordinates c;

    Height h(83);
    print_feet(h);
    return 0;
}
