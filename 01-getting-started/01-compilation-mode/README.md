# The C++ compilation model

To compile the file on Mac use following command:

```
g++ -o HelloUniverse hello-universe.cpp
```

This will create `HelloUniverse` executable, which can run with the following command:

```
./HelloUniverse
```

Expected output is:

```
Hello Universe
```
