#include <iostream>

using namespace std;

// global variable initialisation
int global_var = 100;

void cpp_pointers() {
    // Built-in data types
    int first_variable = 10;
    int& ref_name = first_variable;
    cout << "Value of first_variable: " << first_variable << endl;
    cout << "Value of ref_name: " << ref_name << endl;

    const int imm = 10;
    cout << imm << endl;
    int imm_change = 11;
    cout << imm_change << endl;
    // next line causes an error: cannot change the value of imm
    // imm = imm_change;

    cout << global_var << endl;
    int local_var = 101;
    cout << local_var << endl;

    int global_var = 102;
    cout << "Global: " << ::global_var << endl;
    cout << "Local: " << global_var << endl;
}

void flow_control() {
    // control flow statements
    int x = 10;
    if (x > 0) {
        cout << x << endl;
    }
    if (x > 11) {
        cout << x << endl;
    } else {
        cout << x - 1 << endl;
    }
}

void try_catch() {
    // try-catch
    unsigned count = 0;
    for(unsigned x = 1; x <= 30; x++){
        if (x % 3 == 0) {
            count++;
        }
    }
    cout << count << endl;
}

int main() {
    cpp_pointers();
    cout << global_var << endl;
    // next line doesn't compile, because local_var is out of scope
    // cout << local_var << endl;
    cout << "====" << endl;
    cout << endl;

    flow_control();
    cout << "====" << endl;
    cout << endl;

    try_catch();
    return 0;
}
