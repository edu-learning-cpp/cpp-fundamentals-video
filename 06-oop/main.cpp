#include <iostream>

using namespace std;

// first base class
class Vehicle {
public:
    int getTankCapacity() {
        const int tankLiters = 10;
        cout << "The current tank capacity for your car is " <<
             tankLiters << " Liters." << endl;
        return tankLiters;
    }
};

// second base class
class CollectorItem {
public:
    float getValue() {
        return 100;
    }
};

// Subclass derived from two base classes
class Ferrari250GT : protected Vehicle, public CollectorItem {
public:
    Ferrari250GT() {
        cout << "Thank you for buying the Ferrari 250 GT with tank capacity " << getTankCapacity() << endl;
        //return 0;
    }
};

// Multiple inheritance
class DataScienceDev {
public:
    DataScienceDev(){
        cout << "Welcome to the Data Science Developer Community." << endl;
    }
};

class FutureCppDev {
public:
    FutureCppDev(){
        cout << "Welcome to the C++ Developer Community." << endl;
    }
};

class Student : public DataScienceDev, public FutureCppDev {
public:
    Student(){
        cout << "Student is a Data Developer and C++ Developer." << endl;
    }
};

// virtual method
class Vehicle2 {
public:
    void turnOn() {
        cout << "Vehicle: turn on" << endl;
    }
};

class Car: public Vehicle2 {
public:
    virtual void turnOn() {
        cout << "Car: turn on" << endl;
    }
};

void myTurnOn(Vehicle2& vehicle) {
    cout << "Calling turnOn() on the vehicle reference" << endl;
    vehicle.turnOn();
}

int main() {
    Ferrari250GT ferrari;
    cout << "The value of the Ferrari is " << ferrari.getValue() << endl;
    /* Cannot call ferrari.getTankCapacity() because Ferrari250GT inherits from Vehicle with the protected specifier */

    Student s1;

    Car car;
    myTurnOn(car);
    return 0;
}
