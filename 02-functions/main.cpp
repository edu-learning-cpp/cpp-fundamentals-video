#include <iostream>
#include "log.h"

using namespace std;
// global variables accessible by the whole file.
// Since we don't want to modify them, we mark them with const.
const int POSITION = 10;
const int ALREADY_COMPUTED = 3;

// Define and declare the function
void print_tenth_fibonacci() {
    // Local variables.
    // They don't have to be defined at the top of the function!
//	int n_1 = 1;
//	int n_2 = 0;

    //correction
    int n_1 = 0;
    int n_2 = 1;

    int current = n_1 + n_2;
    // We can access the global variables from inside the function
    for (int i = ALREADY_COMPUTED; i <= POSITION; ++i) {
        current = n_1 + n_2;
        n_2 = n_1;
        n_1 = current;
    }
    cout << current << endl;
}

// Define and declares the function.
// The parameter is taken by value: a new copy will be created.
// Changes inside the function will not be reflected in the value
// used when calling the function
void byvalue_age_in_5_years(int age) {
    // Age here is a new copy. We increment it by 5 but then it's lifetime terminates.
    age += 5;
    cout << "Age in 5 years: " << age << endl;
}

void byreference_age_in_5_years(int& age) {
    age += 5;
}

void modify_pointer(int* pointer) {
    *pointer = 1;
}

void rideRollerCoaster() {
    cout << "yepeee!!!!" << endl;
}

void rideRollerCoasterWithChecks(int heightInCm) {
    if (heightInCm < 100) {
        cout << "Too short" << endl;
        return;
    }
    if (heightInCm > 210) {
        cout << "Too tall" << endl;
        return;
    }
    rideRollerCoaster();
}

// default parameters
int multiply(int multiplied, int multiplier = 1) {
    return multiplied * multiplier;
}

// function overloading
bool isSafeHeightForRollerCoaster(int heightInCm) {
    return heightInCm > 100 && heightInCm < 210;
}

bool isSafeHeightForRollerCoaster(float heightInM) {
    return heightInM > 1.00 && heightInM < 2.10;
}

int main() {
    log();

    print_tenth_fibonacci();

    int age = 95;
    byvalue_age_in_5_years(age);
    // ag3 is not modified because it was accepted by value
    cout << "Current age: " << age << endl; // Prints 95

    age = 13;
    byreference_age_in_5_years(age);
    if (age >= 18) {
        cout << "Congratulations! " << endl;
    }

    int a = 0;
    int* ptr = &a;
    modify_pointer(ptr);
    cout << "Value: " << *ptr << endl;
    cout << "Did the pointer change? " << (ptr == &a) << endl;

    rideRollerCoasterWithChecks(95);
    rideRollerCoasterWithChecks(295);
    rideRollerCoasterWithChecks(195);

    cout << "multiply by default: " << multiply(10) << endl;
    cout << "multiply by 2: " << multiply(10, 2) << endl;

    cout << "is safe: " << isSafeHeightForRollerCoaster(187) << endl;
    cout << "is safe: " << isSafeHeightForRollerCoaster(1.67f) << endl;
    return 0;
}
