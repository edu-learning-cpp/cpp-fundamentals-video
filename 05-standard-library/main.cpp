#include <iostream>
#include <tuple>
#include <optional>
#include <variant>
#include <vector>
#include <iterator>
#include <map>

using namespace std;

struct Visitor {
    void operator()(const string &value) {
        cout << "a string: " << value << endl;
    }

    void operator()(const int &value) {
        cout << "an int: " << value << endl;
    }
};

struct UserAccount {
    int balance;
    int daysSinceRegistered;
};

void computeAnalytics(map<string, UserAccount> &accounts) {
    // Balance of accounts newer than 15 days, in descending order
    vector<UserAccount> newAccounts;
    transform(accounts.begin(), accounts.end(), back_inserter(newAccounts),
              [](const pair<string, UserAccount> &user) { return user.second; });


    auto newEnd = remove_if(newAccounts.begin(), newAccounts.end(),
                            [](const UserAccount &account) { return (account.daysSinceRegistered > 15); });
    newAccounts.erase(newEnd, newAccounts.end());
    sort(newAccounts.begin(), newAccounts.end(),
         [](const UserAccount &lhs, const UserAccount &rhs) { return lhs.balance > rhs.balance; });

    for (const UserAccount &account : newAccounts) {
        cout << account.balance << endl;
    }
}

int main() {
    // Construct a C-string being explicit about the null terminator
    const char charString[8] = {'C', '+', '+', ' ', '1', '0', '1', '\0'};

    // Construct a C-string from a literal string. The compiler
    // automatically adds the \0 at the end
    const char *literalString = "C++ Fundamentals";

    // Strings can be constructed from literal strings.
    string strString = literalString;

    const char *charString2 = strString.c_str();

    cout << charString << endl;
    cout << charString2 << endl;

    // string operations
    string str = "C++ Fundamentals";
    cout << str << endl;

    str.erase(5, 10);
    cout << "Erased: " << str << endl;

    str.clear();
    cout << "Cleared: " << str << endl;

    // tuple
    tuple<string, int, float> james = make_tuple("James", 7, 1.90f);
    cout << "Name: " << get<0>(james) << ". Agent number: " << get<1>(james) <<
         ". Height: " << get<2>(james) << endl;

    // optional
    optional<int> currentHour;
    if (not currentHour.has_value()) {
        cout << "We don't know the time" << endl;
    }
    currentHour = 18;
    if (currentHour) {
        cout << "Current hour is: " << currentHour.value() << endl;
    }

    // variant
    variant<string, int> variant = 42;
    cout << get<1>(variant) << endl;
    cout << get<int>(variant) << endl;

    Visitor visitor;
    cout << "The variant contains ";
    visit(visitor, variant);
    variant = string("Hello world");
    cout << "The variant contains ";
    visit(visitor, variant);

    // iterators
    vector<int> numbers = {1, 2, 3, 4, 5};
    auto it = numbers.begin();
    cout << *it << endl; // dereference: points to 1
    it++; // increment: now it points to 2
    cout << *it << endl;
    // random access: access the 2th element after the current one
    cout << it[2] << endl;
    --it; // decrement: now it points to 1 again
    cout << *it << endl;
    it += 4; // advance the iterator by 4 positions: now it points to 5
    cout << *it << endl;
    it++; // advance past the last element;
    cout << "'it' is after the past element: " << (it == numbers.end()) << endl;

    // reverse iterator
    for (auto rit = numbers.rbegin(); rit != numbers.rend(); ++rit) {
        cout << "Number is: " << *rit << endl;
    }

    numbers = {10, 34, 64, 97, 56, 43, 50, 89, 32, 5};

    for (auto pos = numbers.begin(); pos != numbers.end(); ++pos) {
        cout << "Balance: " << *pos << endl;
    }
    // same as above
    for (int &number : numbers) {
        cout << "Balance: " << number << endl;
    }

    // standard library algorithms
    map<string, UserAccount> users = {
            {"Alice",   UserAccount{500, 15}},
            {"Bob",     UserAccount{1000, 50}},
            {"Charlie", UserAccount{600, 17}},
            {"Donald",  UserAccount{1500, 4}}
    };
    computeAnalytics(users);

    // stream iterator
    auto sit = istream_iterator<int>(cin);
    istream_iterator<int> end;
    for (; sit != end; ++sit) {
        cout << "The number is: " << *sit << endl;
    }
    return 0;
}
